# Jupyterhub-docker with Rstudio

The aim of this project is to run a container with jupyterhub that would launch user docker containers
with jupyterlab and rstudio like [rocker/binder](https://rocker-project.org/images/versioned/binder.html).

Part of the following is a mix between:
- https://jupyterhub.readthedocs.io/en/stable/getting-started
- https://opendreamkit.org/2018/10/17/jupyterhub-docker
- https://jupyterhub-dockerspawner.readthedocs.io/en/latest/docker-image.html
- https://github.com/defeo/jupyterhub-docker/issues/18
- https://jupyterhub.readthedocs.io/en/stable/troubleshooting.html
- https://github.com/jupyterhub/jupyterhub-deploy-docker

As suggested by [playermanny2](https://github.com/jupyterhub/dockerspawner/issues/295#issuecomment-481783620)
we'll start with a simple use case launching jupyter/minimal-notebook to make sure all is working. Then we'll try with
a custom image like rocker/binder.


<p align="center">
    <img src="./readme.img/idg-jupyhterhub-oauht.gif" alt="idg-jupyhterhub-oauht" width="500"/>
</p>

Table of Contents
=================
+ [Start python notebook](#minimal-notebook)
+ [Start rstudio IDE](#rstudio)
+ [Custom configuration](#custom-configuration)
    + [Authentification with Gitlab's OAuth](#adding-gitlab-oauth)
    + [Change Jupyterhub URL for reverse-proxification](#change-hub-url)


## minimal-notebook

The minimal tree is:
```shell
├── docker-compose.yml
├── .env
├── jupyterhub
    ├── Dockerfile
    └── jupyterhub_config.py
```

File `jupyterhub/Dockerfile` contains the recipe for a jupyterhub docker that will manage other dockers. On top of a `jupyterhub/jupyterhub` image, it only needs
dockerspawner to launch a user docker, and a `jupyterhub_config.py` file to configure jupyterhub.

```yaml
# File jupyterhub/Dockerfile
# Do not forget to pin down the version
FROM jupyterhub/jupyterhub:3.0.0

# Download script to automatically stop idle single-user servers
# RUN wget https://raw.githubusercontent.com/jupyterhub/jupyterhub/0.9.3/examples/cull-idle/cull_idle_servers.py

# Install dependencies (for advanced authentication and spawning)
RUN pip3 install dockerspawner
```
__Important:__ one can notice that the jupyterhub version is specified (3.0.0 here). It is so because the hub and
the user containers needs to have the same jupyterhub version to communicate correctly. Otherwise, you may end to a 404
error.
Here the jupyterhub_config.py file is copied inside the image, however it could also be mounted as a volume in order to
have the generic docker image with a dynamic configuration.

File `jupyterhub/jupyterhub_config.py` is containing the configuration of the hub, here only configuring with a docker
spawner. The authenticator could also be configured here. Most of the configuration is defined in environment variables
themselves defined either in files `.env` (automatically loaded by docker-compose) or in the service `jupyterhub` of 
`docker-compose.yml`.

Here, it only contains the image to launch as user container, the network name on which communicate with the 
user containers, the jupyterhub domain where it will be hosted. Several other options are available from the 
[docker spawner api](https://jupyterhub-dockerspawner.readthedocs.io).

```python
# File jupyterhub/jupyterhub_config.py
c = get_config()

import os
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'
c.DockerSpawner.image = os.environ['DOCKER_JUPYTER_IMAGE']
c.DockerSpawner.network_name = os.environ['DOCKER_NETWORK_NAME']
c.JupyterHub.hub_ip = os.environ['HUB_IP']
```

File `.env` is loaded automatically by docker-compose when building or starting the services. Here it only contains
the `COMPOSE_PROJECT_NAME` that will serve as prefix to network name to separate from other possible docker networks.

File `docker-compose.yml` contains:
```yaml
version: '3'

services:
  # Configuration for Hub+Proxy
  jupyterhub:
    image: ${COMPOSE_PROJECT_NAME}_hub_img
    build: jupyterhub                # Build the container from this folder.
    container_name: ${COMPOSE_PROJECT_NAME}_hub    # The service will use this container name.
    volumes:                         # Give access to Docker socket.
      - /var/run/docker.sock:/var/run/docker.sock
      - ./jupyterhub/jupyterhub_config.py:/srv/jupyterhub/jupyterhub_config.py
    environment:                     # Env variables passed to the Hub process.
      DOCKER_JUPYTER_IMAGE: jupyter/minimal-notebook
      DOCKER_NETWORK_NAME: ${COMPOSE_PROJECT_NAME}_default
      HUB_IP: ${COMPOSE_PROJECT_NAME}_hub
    ports:
      - "8000:8000"

  # Configuration for the single-user servers
  jupyterlab:
    image: jupyter/minimal-notebook
    network_mode: none
    command: echo

networks:
  default:
     name: ${COMPOSE_PROJECT_NAME}_default
```

### Build and run

```shell
docker-compose build --no-cache # builds without keeping docker layers in cache
docker-compose up -d # -d option is to detach the services, i.e. run them into the background

# Create a test user with the following
# Of course this is just for the test, as it is absolutly not secure to send some password in clear to the container.
docker exec minimal_notebook_hub sh -c 'useradd -m user1 && echo "user1:user123"|chpasswd'
```

Then try to sign in from the browser at http://localhost:8000 or http://<server_ip>:8000.

__Warning:__ the user containers are persistent so that it is not reinitialized at each start of the hub, 
which is a pretty nice thing. However, it means that if you want to clean completely the hub __and__ the user containers,
you need to remove specifically the user containers in addition to setting down the services.
The following code should remove jupyterhub and user containers respectively.

```shell
docker-compose down && docker rm $(docker ps -qa -f "name=jupyter-")
```

## Rstudio

An Rstudio-session can be launched from inside a jupyterlab session with
[jupyter-rsession-proxy](https://github.com/jupyterhub/jupyter-rsession-proxy).

The docker image [rocker/binder](https://rocker-project.org/images/versioned/binder.html) and 
specially script [install_jupyter.sh](https://github.com/rocker-org/rocker-versioned2/blob/master/scripts/install_jupyter.sh)
gives a good example of how it can be done. Actually it installs JupyterLab, jupyter-rsession-proxy and IRkernel 
on top of the rocker/geospatial image, and defines the default command with jupyterhub-singleuser 
making the container controllable by jupyterhub.

The directory `jupyterhub-rstudio` shows an example of such a configuration.

Following [jupyterhub documentation](https://jupyterhub-dockerspawner.readthedocs.io/en/latest/docker-image.html#how-to-build-your-own-docker-image)
the image rocker/binder is adapted to be spawn by jupyterhub:
```docker
FROM rocker/binder:4.2.1

ARG JUPYTERHUB_VERSION=3.0.0
USER root
RUN pip3 install --no-cache \
    jupyterhub==$JUPYTERHUB_VERSION

USER ${NB_USER}
WORKDIR /home/${NB_USER}
CMD ["jupyterhub-singleuser"]
```
One can notice that no user is created as there is a default user named "rstudio" in rocker images.

A section is then added to `docker-compose.yml` in order to configure the build of rstudio image, named here `rstudio_img`
and its call by jupyterhub.

Use command lines of section [Build and run] to run and access the server. 

## Custom configuration
### Adding GitLab OAuth

Directory `jupyterhub-rstudio-gitlab` is showing an example with on the way to configure jupyterhub in order to call GitLab OAuth 
as authenticator. In order to do so:

- package oautenticator is installed in jupyterhub docker image.
- an application has to be defined in gitlab user settings: it needs scope read_user.
The URI is defined with https://<jupyterhub server>/hub/oauth_callback.
    - <img src="./readme.img/jupyterhub_gitlab_irstea_oauht2.png" alt="jupyterhub_gitlab_irstea_oauht2" width="500"/>
- It's possible to define a self hosted gitlab by using the environnement variable `GITLAB_URL`
- a section is added in jupyterhub_config.py using the URI, the client ID and client secret defined in the gitlab application.
These variables are stored in .env file and called in jupyterhub_config.py.
    - Be **careful**: you should remove the comment (#) at the end of lines.
    - After editing the .env, it has to be sourced: `source .env`

### Change hub URL

The the hub URL can be changed with `bind_url`. Example, if I want to route the hub to http://localhost/minimal_notebook
instead of http://localhost:8000 I could do the following:
```yaml
# File docker-compose.yml
version: '3'

services:
  # Configuration for Hub+Proxy
  jupyterhub:
    image: ${COMPOSE_PROJECT_NAME}_hub_img
    build: jupyterhub                # Build the container from this folder.
    container_name: ${COMPOSE_PROJECT_NAME}_hub    # The service will use this container name.
    volumes:                         # Give access to Docker socket.
      - /var/run/docker.sock:/var/run/docker.sock
      - ./jupyterhub/jupyterhub_config.py:/srv/jupyterhub/jupyterhub_config.py
    environment:                     # Env variables passed to the Hub process.im
      DOCKER_JUPYTER_IMAGE: jupyter/minimal-notebook
      DOCKER_NETWORK_NAME: ${COMPOSE_PROJECT_NAME}_default
      HUB_IP: ${COMPOSE_PROJECT_NAME}_hub
      COMPOSE_PROJECT_NAME: ${COMPOSE_PROJECT_NAME}
    ports:
      - "80:80"

  # Configuration for the single-user servers
  jupyterlab:
    image: jupyter/minimal-notebook
    network_mode: none
    command: echo

networks:
  default:
     name: ${COMPOSE_PROJECT_NAME}_default
```

```python
# jupyterhub_config.yml
c = get_config()

import os
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'
c.DockerSpawner.image = os.environ['DOCKER_JUPYTER_IMAGE']
c.DockerSpawner.network_name = os.environ['DOCKER_NETWORK_NAME']
c.JupyterHub.hub_ip = os.environ['HUB_IP']
c.JupyterHub.bind_url = 'http://0.0.0.0:80/'+os.environ['COMPOSE_PROJECT_NAME']
```

However, with the docker port binding `80:80`, it wouldn't be possible to run another hub in parallel (e.g. for another tutorial).
Thus rerouting outside the container would be a better solution (to be documented).



# Authors

Florian de Boissieu
Rémy Decoupes



