
- Add userlist as a bind volume and use it in jupyterhub_config.py
- Add an ansible playbook to create users for the non-gitlab auth
- for reverse-proxy configuration, check https://jupyterhub.readthedocs.io/en/stable/getting-started/networking-basics.html#adjusting-the-hub-s-url
- remove jupyterhub-rstudio-gitlab/jupyter-rsession-proxy reinstall in rstudio Dockerfile, the fix should be included in future pypi version.
 
