This repo gives an example on how to run a jupyterhub with multiple docker images and
using gitlab as oauth for authentication.

The images are:
- `biodivmapr` that gives an example for an R package
- `eodag`: that gives an example for the jupyterlab eodag-plugin to browse online satellite images
- `pytools4dart`: that gives another example of a python package
- `qgis`: that gives an noVNC example (it could be used for any desktop application)

The images can easily be changed for others, see section [Docker images](#docker-images).

# Usage

## Requirements
This example would need two directories at the root of this project:
- dataparty-hub/DART.tar.gz used by pytools4dart
- dataparty-hub/eodag-cred, credentials for eodag demo

Both directory were needed for the dataparty demo but are private sources.


## Get started

The recommendation would be to build the docker images: `docker-compose build --progress=plain`.
`--progress=plain` prints the full building log to stdout, remove that option if a quiet build is prefered.

Fill `.env` file, creating an application on the gitlab server with asked permission `read_api`, see below.

Start from directory containing `docker-compose.yml` with command line `docker-compose up`.

Access to the hub with `http://<ip addr of server>/dataparty`.
A new container will be runned (spawned) when the user will have authenticate
through gitlab and chosen a specific kernel (or docker image).
Each container is user-specific, thus one user cannot access the content of another user.

Stop the hub with `docker-compose down`, see section [Clean](#clean) to remove artifacts.

# Docker images

The docker images of this example are specified in the directories of `hub/`.
They are then mentionned in `docker-compose.yml` only to rename them with the hub prefix in order to make the cleaning easier.
Finally they are added (if wanted) to the dockerlist file in order to pass them to `jupyterhub_config.py`.

The R package image (`biodivmapr`) is a specific case as it was build upon a [geospatial rocker image](https://github.com/rocker-org/rocker-versioned2)
to avoid specifying all the spatial R packages that were already installed as well as the Rstudio IDE.
As the default notebook user (NB_USER) for that image is `rstudio` and not `jovyan` as usual,
the default user home in the container is `/home/rstudio` and not `/home/jovyan`.
Thus a specific chunk of code was added `jupyterhub_config.py` in order to map the volumes correctly for these containers.

# File .env
This file contains the environment variables passed to docker-compose and
eventually transfered to the jupyterhub container (see docker-compose.yml).

For the current example, it contains:
- the hub name: `COMPOSE_PROJECT_NAME=dataparty`
- the gitlab server used for authentication: `GITLAB_URL="https://gitlab.irstea.fr"`
- the authenticator parameters:
    - `GITLAB_CLIENT_ID`, `GITLAB_CLIENT_SECRET` are given when creating an application on the gitlab.
    - `OAUTH_CALLBACK_URL` is the address where the browser will be redircted once authentified.
    It should be of the form `OAUTH_CALLBACK_URL="http://<server IP address>:8000/dataparty/hub/oauth_callback"`

In order to allow the jupyterhub instance to access the membership of users, the admin has to make an application
on his gitlab profile settings. The gitlab application should ask access for `read_api` in order to
to have access to the details of members of a project or a group. It is what we used here.

# Lists
The directory `lists` contains several lists:
- dockerlist: list of docker images to make available
- user lists: list of gitlab profiles authorized to access the hub
    - userlist: specific users to add
    - grouplist: add gitlab groups members
    - projectlist: add gitlab project members (only members with Developer access or higher)
- adminlist : list of admins of the jupyterhub (not working at the moment)
- idlist : list of correspondance between username and userid in order to give permissions for mounted volumes.
See commented section in `jupyterhub_config.py`.

These lists are parsed in `jupyterhub_config.py` at hub startup.

## Dev user
For development of notebooks, one may need to have a direct bind to the host notebooks with read/write permissions.

That is the objective of the following start method upgraded from default:
```python
from dockerspawner import DockerSpawner
class MyDockerSpawner(DockerSpawner):
    team_map = {
        'floriandeboissie': 'dev'
    }

    def start(self):
        if self.user.name in self.team_map and self.team_map[self.user.name]=='dev':

            # add team volume to volumes
            self.volumes.pop('{prefix}-{username}-notebooks')
            self.volumes['{prefix}-notebooks'] = {
                'bind': '/home/jovyan/notebooks',
                'mode': 'rw',  # or ro for read-only
            }

        return super().start()

c.JupyterHub.spawner_class = MyDockerSpawner

```

# Volumes

In this example, the only volume that is mounted is the directory `notebooks`.

There are two ways it could mounted:
- with read-only permissions: in that case the user can modify & execute the notebooks, but not save the modifications.
- with read/write permissions: in that case the volume must be specific to each user.

## Read-only shared volume

The same volume is shared by all users and the `jupyterhub_config.py` would contain:
```python
c.DockerSpawner.volumes = {
    '{prefix}-notebooks': {'bind': '/home/jovyan/notebooks', 'mode':'ro'},
}
```

## Read/write user-specific volume
In order to have a user-specific volume, `jupyterhub_config.py` would contain:
```python
c.DockerSpawner.volumes = {
     '{prefix}-{username}-work': {'bind': '/home/jovyan/work', 'mode':'rw'},
}
```
At user first access, this volume will be created (empty) and will be kept persitent for future access (i.e. the volume is not removed when shuting down the hub, and connected when login again), unless explicitly removed by hub admin (see section [Clean](#clean)).

## User-specific editable notebooks
In the case the volume destination directory already exists in the image, the directory content will be copied to the user-specific mounted volume.
It is the way I found to have user-specific editable notebooks:
1. include the directory into the docker image, e.g. adding those lines to the wanted Dockerfile:
```dockerfile
COPY ../../notebooks /home/jovyan/notebooks
```
2. add it as a R/W volume in `jupyterhub_config.py` (notice the `{username}` tag in the volume naming):
```python
c.DockerSpawner.volumes = {
    '{prefix}-{username}-notebooks': {'bind': '/home/jovyan/notebooks', 'mode':'rw'},
}
```
This way the user specific notebooks volume is initialized only once with the content of the directory/file at the path it is 
mounted, and then is persistent (i.e. not created again when logging in again).

Another way to do that would be to copy the notebook content at the user session startup,
e.g. modifying the `start` method like in section [Dev user](#dev-user).

## Specific volumes
Example, if you want to bind volumes to a host directory only for dev (i.e. editable notebooks with changes saved on the host disk),
add to the begining of `jupyterhub_config.py`:
```shell
# inpired from https://github.com/jupyterhub/dockerspawner/issues/239
from dockerspawner import DockerSpawner
class MyDockerSpawner(DockerSpawner):
    team_map = {
        'floriandeboissie': 'dev'
    }
    # pre-start redefinition of volumes
    def start(self):
        if self.user.name in self.team_map and self.team_map[self.user.name]=='dev':

            # add team volume to volumes
            self.volumes.pop('{prefix}-{username}-notebooks')
            self.volumes['{prefix}-notebooks'] = {
                'bind': '/home/jovyan/notebooks',
                'mode': 'rw',  # or ro for read-only
            }

        return super().start()

c.JupyterHub.spawner_class = MyDockerSpawner
```


# Clean
In `jupyterhub_config.py` it is possible to specify the hub to remove automatically the containers at the hub shutdown:
```python
c.DockerSpawner.remove=True
```

However, if the hub does not manage to stop correctly a container, it may be necessary to remove them by hand.
Moreover, the volumes are persitent, thus removing them would have to be by hand also.

This section shows how to clean the jupyterhub element not removed, after shut down.
The examples below suppose the prefix (or COMPOSE_PROJECT_NAME in .env) is `dataparty`.

Containers:
```shell
docker-compose down && docker rm $(docker ps -qa -f "name=dataparty-")
```

Volumes: 
```shell
docker volume rm $(docker volume ls -f "name=dataparty" -q)
```

images:
```shell
# list images
docker images -f "reference=dataparty-*"
docker rmi $(docker images -f "reference=dataparty-*")
```


# Troubleshooting

A list of issues that could be encountered.

## .External2(C_savehistory, file) : No such file or directory

The problem comes from permission to write .Rhistory in current directory

## QGIS

Works on http, but does not seem to work on https reverse proxy, maybe it needs a specific reverse proxy config:
https://stackoverflow.com/questions/27721855/trying-to-get-websockify-novnc-to-work-through-a-reverse-proxy
