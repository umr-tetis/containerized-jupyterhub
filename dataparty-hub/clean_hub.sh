#!/bin/bash

if [ $# -eq 0 ]
then
	echo "Usage: "
	echo "  ./clean_hub.sh [options]"
	echo "Options: "
	echo "  containers	remove hub containers"
	echo "  volumes		remove hub volumes"
	echo "  all			remove hub containers & volumes"
elif [ $# -eq 1 ]; then
	if [ "$1" = 'containers' ]; then
		echo "Removing hub containers."
		docker-compose -f ./hub/docker-compose.yml down && \
		docker rm $(docker ps -qa -f "name=dataparty-")
	elif [ "$1" = 'volumes' ]; then
		echo "Removing hub volumes..."
		docker volume rm $(docker volume ls -f "name=dataparty" -q)
	elif [ "$1" = 'all' ]; then
		echo "Removing hub containers..."
		docker-compose -f ./hub/docker-compose.yml down && \
		docker rm $(docker ps -qa -f "name=dataparty-")
		echo "Removing hub volumes..."
		docker volume rm $(docker volume ls -f "name=dataparty" -q)
	fi
fi
