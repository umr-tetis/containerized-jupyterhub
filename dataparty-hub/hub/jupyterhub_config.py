# c = get_config()
from traitlets import Unicode
import os
from dockerspawner import DockerSpawner

# inpired from https://github.com/jupyterhub/dockerspawner/issues/239
class MyDockerSpawner(DockerSpawner):
    team_map = {
        'floriandeboissieu': 'dev'
    }

    def start(self):
        if self.user.name in self.team_map and self.team_map[self.user.name]=='dev':

            ### Make notebook editable by dev
            if '{prefix}-{username}-notebooks' in self.volumes:
                self.volumes.pop('{prefix}-{username}-notebooks')
            self.volumes['{prefix}-notebooks'] = {
                'bind': f'/home/jovyan/notebooks',
                'mode': 'rw',  # or ro for read-only
            }

        ### specify options specific to some image
        # Here for example, biodivmapr image is based on rocker image which has rstudio as the default user name.
        # Thus the target of mounted volumes is changed to mount them in /home/rstudio
        image_option = self.user_options.get('image')
        # self.log.info(f'image is: {image_option}')
        if image_option == "biodivmapr":
            for k in self.volumes:
                self.volumes[k]['bind'] = self.volumes[k]['bind'].replace('jovyan', 'rstudio')

        return super().start()
    #### this ccommented chunk can be uncommented to change NB_UID, i.e. user ID within the container ####
    # def pre_spawn_hook(self, spawner):
    #     # TODO: instead of setting notebook inside image,
    #     #  1. mount notebook in hub container
    #     #  2. use pre_spawn_hook to copy notebook in user volume
    #     #  3. post_hook to remove it
    #     #  --> notebook could be dynamic without recompiling the image
    #     #  see https://github.com/jupyterhub/jupyterhub/tree/main/examples/bootstrap-script
    #     self.log.info(f'user is {self.user.name}')

    #     # WARNING: changing NB_USER will copy recursively jovyan to /home/$NB_USER
    #     # self.environment = {**self.environment, 'NB_USER': self.user.name}
    #     # The following gives an example for specifying user ID (e.g. for specific permissions on files)
    #     pwd = os.path.dirname(__file__)
    #     idlist_file = os.path.join(pwd, 'idlist')
    #     if os.path.isfile(idlist_file):
    #         with open(idlist_file) as f:
    #             for line in f:
    #                 if not line or line.startswith('#'):
    #                     continue
    #                 parts = line.split()
    #                 # in case of newline at the end of userlist file
    #                 if len(parts)==2 and parts[0] == self.user.name:
    #                     self.log.info(f"Adding idlist user: {parts[0]}")
    #                     self.environment["NB_UID"]=parts[1]
    #                     # root is necessary for jupyter notebooks docker images, to adapt for rocker images.
    #                     self.extra_create_kwargs = {'user' : '0'}

    #         self.log.info(f'Chnaged NB_UID to {parts[1]}')

c.JupyterHub.spawner_class = MyDockerSpawner

c.DockerSpawner.network_name = os.environ['DOCKER_NETWORK_NAME']
c.JupyterHub.hub_ip = os.environ['HUB_IP']
c.JupyterHub.bind_url = 'http://0.0.0.0:8000/'+os.environ['URL_PREFIX']

c.DockerSpawner.name_template = "{prefix}-{imagename}-{username}-{servername}"
c.DockerSpawner.remove=True # delete containers when servers are stopped. 

prefix = os.environ['URL_PREFIX']
c.DockerSpawner.prefix=prefix

eodag_dir = '/eodag'

# Volumes for default users
c.DockerSpawner.volumes = {
    # WARNING: mounting eodag credentials as such will make them available to any user... 
    # So do not put your own, it is just for the example.
    '{prefix}-eodag': {'bind': eodag_dir, 'mode':'rw'}, # needs to be r/w otherwise eodag server does not start
    '{prefix}-notebooks': {'bind': '/home/jovyan/notebooks', 'mode':'ro'},
    '{prefix}-{username}-work': {'bind': '/home/jovyan/work', 'mode':'rw'},
}

# limits
c.JupyterHub.allow_named_servers = True # set to False in order to avoid a user to run another server then his main one
c.JupyterHub.named_server_limit_per_user=2 
c.DockerSpawner.mem_limit='32G'
c.DockerSpawner.cpu_limit=8

# list of available singleuser dockers
pwd = os.path.dirname(__file__)
c.DockerSpawner.allowed_images = dockers = dict()
image_prefix = prefix+'-'
if os.path.isfile('dockerlist'):
    with open(os.path.join(pwd, 'dockerlist')) as f:
        for line in f:
            if not line or line.startswith('#'):
                continue
            parts = line.split()
            if len(parts) == 1:
                dockers[parts[0]]=parts[0]
            elif len(parts) > 1:
                dockers[parts[0]]=parts[1]
if len(dockers)==1:
    c.DockerSpawner.image = list(dockers.values())[0]
    dockers={}


# Whitlelist users, grouplist and admins
# inpired from https://github.com/jupyterhub/jupyterhub-deploy-docker/blob/master/jupyterhub_config.py
# Blacklists could also be set with c.Authenticator.blocked_users (see https://jupyterhub.readthedocs.io)
## Configure authentication (delegated to GitLab)
## https://oauthenticator.readthedocs.io/en/latest/getting-started.html#general-setup
from oauthenticator.gitlab import GitLabOAuthenticator
c.JupyterHub.authenticator_class = GitLabOAuthenticator
c.GitLabOAuthenticator.oauth_callback_url = os.environ['OAUTH_CALLBACK_URL']
c.GitLabOAuthenticator.client_id = os.environ['GITLAB_CLIENT_ID']
c.GitLabOAuthenticator.client_secret = os.environ['GITLAB_CLIENT_SECRET']
c.GitLabOAuthenticator.scope = ['read_api'] # level necessary for to read users details, i.e. username
c.Authenticator.allowed_users = whitelist = set()
c.Authenticator.admin_users = admin = set()
c.GitLabOAuthenticator.allowed_project_ids = projects = set()
c.GitLabOAuthenticator.allowed_gitlab_groups = groups = set()
c.JupyterHub.admin_access = False
if os.path.isfile('grouplist'):
    with open(os.path.join(pwd, 'grouplist')) as f:
        for line in f:
            if not line or line.startswith('#'):
                continue
            parts = line.split()
            # in case of newline at the end of userlist file
            if len(parts) >= 1:
                name = parts[0]
                groups.add(name)
if os.path.isfile('adminlist'):
    with open(os.path.join(pwd, 'adminlist')) as f:
        for line in f:
            if not line or line.startswith('#'):
                continue
            parts = line.split()
            # in case of newline at the end of userlist file
            if len(parts) >= 1:
                name = parts[0]
                admin.add(name)
if os.path.isfile('userlist'):
    with open(os.path.join(pwd, 'userlist')) as f:
        for line in f:
            if not line or line.startswith('#'):
                continue
            parts = line.split()
            # in case of newline at the end of userlist file
            if len(parts) >= 1:
                name = parts[0]
                whitelist.add(name)
if os.path.isfile('projectlist'):
    with open(os.path.join(pwd, 'projectlist')) as f:
        for line in f:
            if not line or line.startswith('#'):
                continue
            parts = line.split()
            # in case of newline at the end of userlist file
            if len(parts) >= 1:
                name = parts[0]
                projects.add(name)

