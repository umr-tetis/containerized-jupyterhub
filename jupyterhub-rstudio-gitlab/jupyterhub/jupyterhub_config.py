c = get_config()

import os
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'
c.DockerSpawner.image = os.environ['DOCKER_JUPYTER_IMAGE']
c.DockerSpawner.network_name = os.environ['DOCKER_NETWORK_NAME']
c.JupyterHub.hub_ip = os.environ['HUB_IP']

## Configure authentication (delagated to GitLab)
## https://oauthenticator.readthedocs.io/en/latest/getting-started.html#general-setup
from oauthenticator.gitlab import GitLabOAuthenticator
c.JupyterHub.authenticator_class = GitLabOAuthenticator
c.GitLabOAuthenticator.oauth_callback_url = os.environ['OAUTH_CALLBACK_URL']
c.GitLabOAuthenticator.client_id = os.environ['GITLAB_CLIENT_ID']
c.GitLabOAuthenticator.client_secret = os.environ['GITLAB_CLIENT_SECRET']
c.GitLabOAuthenticator.scope = ['read_user'] # level necessary for to read users
# c.GitLabOAuthenticator.allowed_users = ['<username1>', '<username2>'] # to allow specific users
# c.GitLabOAuthenticator.allowed_groups = ['<group1>', '<group2>'] # to allow whole groups



